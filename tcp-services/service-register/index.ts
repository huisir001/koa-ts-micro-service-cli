/*
 * @Description: 入口
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-22 10:30:44
 * @LastEditTime: 2022-07-07 13:41:32
 */
import Micro, { useConfig, print } from 'tcp-micro-service'
import init from './middleware/init.js'
import Register from './controller/Register.js'

// 读取配置
const { server } = useConfig()

const micro = new Micro(server.name)

micro
    .listen(server, (res) => {
        const { address, port } = res.address
        if (res.t === 1) {
            print.info(`微服务 ${res.name} 启动成功！服务地址：${address}:${port}`,)
        }

        if (res.t === 2) {
            print.info(`有新的客户端 ${res.name} 连接成功！`)
        }
    })
    // 初始化
    .use(init, micro)
    // 控制器
    .controllers(Register)
    // 错误统一处理
    .onError((error) => {
        print.error(error.message)
    })