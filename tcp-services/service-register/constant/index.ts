/*
 * @Description: 常量
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-25 17:20:11
 * @LastEditTime: 2022-05-17 16:46:09
 */
import { useConfig } from 'tcp-micro-service'

const { common } = useConfig()

const CONST = {
    // 服务注册表key
    KEY_SERVICE_REG: common.tablePrefix + 'SERVICE_REGISTRY',
}

export default CONST