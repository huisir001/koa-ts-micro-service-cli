/*
 * @Description: 数据传输对象
 * @Autor: HuiSir<www.zuifengyun.com>
 * @Date: 2022-07-07 17:56:04
 * @LastEditTime: 2022-07-07 18:56:32
 */
import { usePropDecorators, useClassDecorators, IServerOpts } from "tcp-micro-service"

const { Dto } = useClassDecorators()
const { Require, Body, Param, IsString, IsNumber } = usePropDecorators()

/**
 * 注册
 */
@Dto
export class SaveDto implements IServerOpts {
    @Require
    @Body
    @IsString
    name: string // 微服务名称（唯一标识）

    @Require
    @Body
    @IsNumber
    port: number

    @Body
    @IsString
    pin: string  // 秘钥

    @Body
    @IsString
    host: string
}

/**
 * 查询
 */
@Dto
export class FindDto {
    @Require
    @Param
    @IsString
    name: string
}
