/*
 * @Description: 初始化
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-22 10:41:24
 * @LastEditTime: 2022-05-25 15:45:18
 */
import Micro, { useRedisPool, print } from 'tcp-micro-service'
import CONST from '../constant/index.js'

// redis连接池
const redisPool = useRedisPool()

/**
 * 初始化
 * 定期检测已缓存的服务状态，失效的删除
 */
export default (micro: Micro) => {
    // 是否初次启动
    // let setupFlag: boolean = false;
    (async function checkService() {
        // 取连接
        const redis = await redisPool.getConn()
        const serviceList = await redis.hgetall(CONST.KEY_SERVICE_REG)
        // 初次启动删除所有缓存
        // if (!setupFlag) {
        for (let serviceName in serviceList) {
            await redis.hdel(CONST.KEY_SERVICE_REG, serviceName)
        }
        // setupFlag = true
        // }
        // else {
        //     // 遍历所有缓存的服务数据
        //     for (let serviceName in serviceList) {
        //         const { host, port, pin } = JSON.parse(serviceList[serviceName])
        //         // 验证tcp
        //         micro.client({ host, port, name: micro.name, pin }, async (err, socket) => {
        //             if (err) {
        //                 // 请求失败,删除相应数据
        //                 await redis.hdel(CONST.KEY_SERVICE_REG, serviceName)
        //             }
        //             socket.end()
        //         })
        //     }
        // }
        // 释放
        redis.release()
        // 定时检查：5分钟
        // setTimeout(checkService, 1000 * 60 * 5)
    })()

    // print.info('已启动微服务定期检测！')
}