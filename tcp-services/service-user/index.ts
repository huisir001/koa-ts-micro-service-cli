/*
 * @Description: 入口
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-22 10:30:44
 * @LastEditTime: 2022-07-07 13:52:39
 */
import Micro, { useConfig, register, print } from 'tcp-micro-service'
import controller from './controller/index.js'

// 读取配置
const { server } = useConfig()

const micro = new Micro(server.name)

micro
    .listen(server, (res) => {
        const { address, port } = res.address
        if (res.t === 1) {
            print.info(`微服务 ${res.name} 启动成功！服务地址：${address}:${port}`,)
        }

        if (res.t === 2) {
            print.info(`有新的客户端 ${res.name} 连接成功！`)
        }
    })
    // 注册到注册中心
    .use(register)
    // 控制分发
    .controllers(controller)
    // 错误统一处理
    .onError((error) => {
        print.error(error.message)
    })