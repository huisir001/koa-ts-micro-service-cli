/*
 * @Description: 手写字符串对称加密
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2021-05-26 18:09:58
 * @LastEditTime: 2022-05-25 16:48:15
 */
import crypto from 'crypto';

class Encrypt {
    Skey: string

    /**
     * @param {string} skey 秘钥
     */
    constructor(Skey: string) {
        this.Skey = Skey
    }

    /**
     * @description: 加密
     * @param {string} src 要加密的字符串
     * @return {string}
     * @author: HuiSir
     */
    encode(src: string | number): string {
        src = src.toString()
        // 加盐
        const iv = Buffer.alloc(0);
        const keyBuffer = crypto.createHash('md5').update(this.Skey).digest()
        const cipher = crypto.createCipheriv('AES-128-ECB', keyBuffer, iv);
        return cipher.update(src, 'utf8', 'hex') + cipher.final('hex');
    }

    /**
    * @description: 解密
    * @param {string} sign 要解密的密文
    * @return {string}
    * @author: HuiSir
    */
    decode(sign: string): string {
        // 加盐
        const iv = Buffer.alloc(0);
        const keyBuffer = crypto.createHash('md5').update(this.Skey).digest()
        const cipher = crypto.createDecipheriv('AES-128-ECB', keyBuffer, iv);
        return cipher.update(sign, 'hex', 'utf8') + cipher.final('utf8');
    }
}

export default new Encrypt("sadjnksadjnsakdj")