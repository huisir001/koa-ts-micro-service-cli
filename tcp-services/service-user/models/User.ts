/*
 * @Description: 用户Model
 * @Autor: HuiSir<www.zuifengyun.com>
 * @Date: 2022-05-27 18:51:15
 * @LastEditTime: 2022-07-22 10:26:11
 */
import { createSqlModel } from "tcp-micro-service"

// 默认主键id自动添加
// 默认已添加创建时间（create_time）和更新时间（update_time），这里也无需再配置
// 一般有默认值的 都需要设notNull: true
const userModel = createSqlModel('user', {
    username: {
        type: 'VARCHAR(255)',
        notNull: true,
        comment: '用户名'
    },
    password: {
        type: 'VARCHAR(255)',
        notNull: true,
        comment: '密码'
    },
    sex: {
        type: 'TINYINT', //0为女性，1为男性
        default: 0, //默认值
        notNull: true,
        comment: '性别'
    },
    status: {
        type: 'TINYINT', //小整数
        default: 0,
        notNull: true,
        comment: '状态'
    }
})

export default userModel