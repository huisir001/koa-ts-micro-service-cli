/*
 * @Description: 传参对象
 * @Autor: HuiSir<www.zuifengyun.com>
 * @Date: 2022-07-05 10:10:21
 * @LastEditTime: 2022-07-07 22:48:16
 */
import { usePropDecorators, useClassDecorators, useConfig } from "tcp-micro-service"

const { Dto } = useClassDecorators()
const { Require, Body, Query, IsString, IsNumber, Header } = usePropDecorators()

/**
 * 注册
 */
@Dto
export class SignupDto {
    @Require
    @IsString
    @Body
    username: string

    @Require
    @IsString
    @Body
    password: string

    @IsNumber
    @Body
    sex: number
}

/**
 * 登录
 */
@Dto
export class LoginDto {
    @Require
    @Body
    @IsString
    username: string

    @Require
    @Body
    @IsString
    password: string
}

/**
 * ID查询用户
 */
@Dto
export class FindUserByIdDto {
    @Require
    @IsString
    @Query
    id: string
}