# koa-ts-micro-service-cli
Node 微服务轻量级脚手架    

NodeJS在I/O密集型应用中所表现的突出能力使得越来越多的开发者青睐。如今越来越多的应用涉及到的都是I/O方面的业务，故而NodeJS在性能和学习成本方面展现了很大优势。在微服务架构的席卷下，甚至好多大型应用会采用“NodeJS+其他开发语言”这种多语言开发模式进行开发。所以“NodeJS微服务开发”是很有必要的。    

## 框架选型

先前了解到node有一微服务框架名为`seneca`，于是尝试一番，由于其插件和本身的维护都比较旧，用法也很麻烦。最终由于其日志功能、数据库支持、与PM2的搭配、与redis的搭配等方面都不尽如人意，固放弃。   

遂了解到有一款较为完善的微服务框架`Moleculer`，看了官方文档感觉很复杂，学习成本较高，于是想要按自己的理解写一个轻量级的较为简单的脚手架。    

微服务的核心是：主服务和各个微服务之间的通信问题，涉及到发布订阅功能。在`Moleculer`框架指导下，了解到有很多插件能够解决，比如：`TCP`、`NATS`、`Redis`、`MQTT`、`AMQP`、`Kafka`等，通过查阅，`TCP`方案可以直接使用Node内置`net`模块的`tcp`功能进行封装，而`Redis`是在服务中必用的缓存模块，其自带的发布订阅功能的API也很简单。所以这两种方案都能用。`Redis`可以直接使用`ioredis`模块，无需封装。   

了解到，如果使用redis方案的话，若所有微服务均在同一台服务器，那么只使用一个redis服务，那么不易实现多端口的微服务方案，且若在不同服务器的话，如果某个微服务没有使用redis缓存功能，那么只使用redis的发布订阅功能就显得很浪费。所以这里果断采用`TCP`方案，自己封装一个发布订阅模式。   

## 主要依赖\技术栈

- TypeScript：使用TS方便函数和参数的类型检测
- Koa2：一种以AOP(面向切面编程)的web开发框架，主要思路为洋葱模型和中间件架构。这款框架自己较为熟悉，用于主服务。
- [tcp-micro-service](https://gitee.com/huisir001/tcp-micro-service.git)：个人开发的一款用于微服务的TCP发布订阅通信工具，[GIT地址](https://gitee.com/huisir001/tcp-micro-service.git)
- PM2：强大的Nodejs应用部署器（先前自己项目使用pkg模块进行打包后再进行部署，在linux下需要使用nohub命令，在win下需要生成nohub.vbs脚本进行执行，而在微服务框架下，打包多个应用后各自运行不便于集中管理，所以还是直接使用pm2）   

## 架构

| 名称          | 技术模式/框架 | 说明                                                   |
| ------------- | ------------- | ------------------------------------------------------ |
| 网关/负载均衡 | Nginx         | 对前后端的沟通进行统一的管理                           |
| 配置中心      | service-config服务(内置)  | 所有服务的配置统一管理，各服务会定期pull各自相应的配置参数到缓存中（配置中心初步使用yaml文件进行配置，各服务也暂用yaml文件进行缓存，后期可更改为数据库和redis）  |
| 传输器 | [tcp-micro-service](https://gitee.com/huisir001/tcp-micro-service.git) | 多个微服务节点之间进行沟通，采用发布订阅模式 |
| 注册中心/中央消息转发器 | service-register服务(内置)  | 对所有微服务配置进行管理，服务必须注册登记才能使用，主服务接口请求时候进行client,只在第一次请求时client       |
| 微服务        | net/tcp        | 创建各种单一职责和接口分离模块(插件)，各微服务可分布式部署到不同服务器 |

## 内置服务/功能

| 功能   | 服务/框架 | 端口 |
| ------ | --------- | ---- |
| 数据库 | Mysql     |      |
| 数据模型/实体 |   |      |
| 缓存库 | Redis(这里使用redis而不用其他库，因为redis便于后期管理)     |      |
| 日志   | log4js，配合PM2插件输出到文件 |      |

## 配置文件

yaml配置文件阅读方便，支持多种类型（字符串、数值、布尔值、null、时间戳、数组），可注释，故而用途广泛，比json和ini等好太多。   
Nodejs读写yaml需要用到yaml库。
```js
import YAML from 'yaml'
// 读取
const file = fs.readFileSync('./config.yaml', 'utf8')
const yamlStr = YAML.parse(file)
console.log(yamlStr)
// 写入
const options = {a:1, b:2}
fs.writeFileSync('config.yaml', YAML.stringify(options))
```

## 启动