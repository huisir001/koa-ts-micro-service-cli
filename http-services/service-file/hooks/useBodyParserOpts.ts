/*
 * @Description: bodyparser配置
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-22 16:21:18
 * @LastEditTime: 2022-07-29 16:03:32
 */
import { type bodyParser } from 'koa-body-2'
import useAssetsPath from './useAssetsPath.js'

export default (): bodyParser.IOptions => {
    return {
        multipart: true,
        multiOptions: {
            maxFiles: 5, // 多文件上传数量限制
            maxFileSize: 100 * 1024 * 1024, // 文件大小限制 100M
            uploadDir: useAssetsPath(), // 上传位置
            deleteTimeout: 1000 * 60 * 10, // 文件超时删除，这里定时10分钟（太短可能会在文件未上传完成时删除，太长容易占用太多内存）
            // deleteTimeout: 8000,
        }
    }
}