/*
 * @Description: 文件上传路径
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-07-20 10:51:46
 * @LastEditTime: 2022-07-20 11:01:46
 */
import { useConfig } from 'tcp-micro-service'
import path from 'node:path'
import os from 'node:os'

const { common: { assetsPath } } = useConfig()

/**
 * 由于在koa-body-2中上传文件时已经对路径进行创建（不存在路径则创建相应文件夹）
 * 所以在这里不重复处理
 */
export default () => {
    // 格式化
    const normalPath = path.normalize(assetsPath)
    // 判断是绝对还是相对路径
    if (new RegExp(`^[a-zA-Z]:(\\${path.sep}.+)+$`).test(normalPath)) {
        // 绝对路径
        return normalPath
    } else {
        return path.join(os.homedir(), normalPath)
    }
}