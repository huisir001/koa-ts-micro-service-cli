/*
 * @Description: 文件Model
 * @Autor: HuiSir<www.zuifengyun.com>
 * @Date: 2022-05-27 18:51:15
 * @LastEditTime: 2022-07-22 15:37:30
 */
import { createSqlModel } from "tcp-micro-service"

// 这里是整个文件模型
// 文件所有片段上传完毕将得到一个完整的文件（根据分片的下标（位置）和size往单个文件中写入）

// 默认主键id自动添加
// 默认已添加创建时间（create_time）和更新时间（update_time），这里也无需再配置
// 一般有默认值的，都需要设notNull: true
const fileModel = createSqlModel('file', {
    /**
     * 文件名（原始，带文件扩展名）
     */
    name: {
        type: 'VARCHAR(255)',
        notNull: true,
        comment: '文件名（原始，带文件扩展名）'
    },

    /**
     * 文件扩展名，如`.exe`、`.xml`
     */
    extName: {
        type: 'VARCHAR(36)',
        notNull: true,
        comment: '文件扩展名，如`.exe`、`.xml`'
    },

    /**
     * 文件名（重设后，存储名称）
     */
    newName: {
        type: 'VARCHAR(120)',
        comment: '文件名（重设后，存储名称）'
    },

    /**
     * The size of the uploaded file in bytes. If the file is still being uploaded,
     * this property says how many bytes of the file have been written to disk yet.
     */
    size: {
        type: 'INT',
        comment: '文件大小，单位byte'
    },

    /**
     * 带单位的文件大小，保留2位小数，如`100.11 KB`、`100.12 MB`,单位只限KB、MB、GB
     */
    unitSize: {
        type: 'VARCHAR(36)',
        comment: '带单位的文件大小，保留2位小数，如`100.11 KB`、`100.12 MB`,单位只限KB、MB、GB'
    },
    /**
     * 绝对路径（本地存储），非本地存储可为空
     */
    path: {
        type: 'VARCHAR(255)',
        comment: '绝对路径'
    },

    /**
     * 相对路径、外链路径
     * 便于数据库存储和前台访问(前端使用，因为path为绝对路径不安全)
     */
    src: {
        type: 'VARCHAR(255)',
        comment: '相对路径'
    },

    /**
     * The mime type of this file, according to the uploading client.
     */
    type: {
        type: 'VARCHAR(120)',
        comment: '文件 mime type'
    },


    /**
     * 最后一次修改时间戳，毫秒数 (文件上传后需要修改文件名再存储，所以取Date.now()为最后修改时间)
     * @default `Date.now()`
     */
    lastModified: {
        type: 'BIGINT',
        notNull: true,
        comment: '最后一次修改时间戳，毫秒数'
    },

    /**
     * 文件唯一标识
     * 将作为文件存储的新文件名
     */
    chunkId: {
        type: 'VARCHAR(120)',
        notNull: true,
        comment: '文件唯一标识'
    },


    md5: {
        type: 'VARCHAR(255)', // md5的固定长度是128位
        comment: '文件md5'
    },

    /**
     * 文件描述
     */
    description: {
        type: 'VARCHAR(255)',
        comment: '文件描述，介绍'
    },

    /**
     * 文件状态（针对整个文件，非文件片段）
     * 已删除：-1
     */
    status: {
        type: 'TINYINT', //小整数
        default: 0,
        notNull: true,
        comment: '状态'
    }
})

export default fileModel