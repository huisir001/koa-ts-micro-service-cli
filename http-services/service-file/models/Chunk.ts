/*
 * @Description: 分片缓存表
 * @Autor: HuiSir<github.com/huisir001>
 * @Date: 2022-07-22 15:09:24
 * @LastEditTime: 2022-08-01 17:34:09
 */
import { createSqlModel } from "tcp-micro-service"

// 分片合并完毕后会删除相应分片及缓存数据
// 分片缓存之所以不使用redis是为了持久化存储，便于持久化的断点续传

export default createSqlModel('chunk', {
    md5: {
        type: 'VARCHAR(255)', // md5的固定长度是128位
        comment: '文件md5'
    },

    chunkNum: {
        type: 'INT',
        comment: '当前切片序号'
    },

    chunkCount: {
        type: 'INT',
        comment: '切片总数'
    },

    chunkSize: {
        type: 'INT', // 字节数
        notNull: true,
        comment: '当前切片大小'
    },

    limitSize: {
        type: 'INT',
        comment: '切片标准大小'
    },

    /**
    * 绝对路径（本地存储），非本地存储可为空
    */
    path: {
        type: 'VARCHAR(255)',
        comment: '绝对路径'
    },
})