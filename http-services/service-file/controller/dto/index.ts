/*
 * @Description: 传参对象
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-07-20 14:40:21
 * @LastEditTime: 2022-08-02 11:22:32
 */
import { useClassDecorators, usePropDecorators, type KoaControl } from 'koa-control'

const { Dto } = useClassDecorators()
const { Require, Param, Body, Raw, Files, IsString } = usePropDecorators()

/**
 * 单文件访问、下载
 * Param参数都是字符串，id若为数字需转类型
 */
@Dto
export class AccessDto {
    @Require
    @Param
    id: string
}

/**
 * 文件打包下载
 */
@Dto
export class DownloadZipDto {
    @Require
    @Body
    @IsString
    ids: string
}

/**
 * 查询文件上传情况
 */
@Dto
export class FileStatusDto {
    @Require
    @Param
    md5: string
}

/**
 * 普通上传
 */
@Dto
export class UploadDto {
    @Require
    @Files
    file: KoaControl.Files
}

/**
 * 分片上传
 * Raw 无需限制数据类型，都是字符串
 */
@Dto
export class SlicedUploadDto {
    // 文件对象
    @Require
    @Files
    chunk: KoaControl.File
    // 文件(整体)md5，非分片md5（没必要）
    @Require
    @Raw
    md5: string
    // 当前切片序号（从0开始）
    @Require
    @Raw
    chunkNum: number
    // 切片总数
    @Require
    @Raw
    chunkCount: number
    // 每片大小（切片标准）
    @Require
    @Raw
    limitSize: number
}