# 文件服务

## 功能

1. 图片、普通文件增删改查
2. 图片查看、文件下载
3. 组内（用户）文件打包下载
4. 文件分段上传、断点续传、断点下载
5. 文件分类标签
6. 用户文件夹、公共文件夹

## 文件详情字段

1. 创建时间、更新时间
2. 文件大小
3. 图片尺寸（图片类型可能需要）
4. 文件URL（图片地址、文件下载地址）
5. 文件名称
6. 文件类目（标签，可新增，不可删除和更改）
7. 文件描述（备注）
8. 所属（用户、公共）