/*
 * @Description: 请求验证、访问过滤
 * @Autor: HuiSir<www.zuifengyun.com>
 * @Date: 2022-05-25 13:48:57
 * @LastEditTime: 2022-07-22 17:25:25
 */
import type Koa from 'Koa'
import Micro, { useConfig, resErr } from 'tcp-micro-service'
import useUserServiceClient from '../hooks/useUserServiceClient.js'

export default (): Koa.Middleware<Promise<void>> => {
    const { apiWhiteList, apiBlackList } = useConfig().common

    return async (ctx, next) => {
        // 白名单放行
        if (apiWhiteList?.find((item: string) => ctx.originalUrl.includes(item))) {
            await next() // 注意别忘了await
            return
        }

        // 黑名单阻断
        if (apiBlackList?.find((item: string) => ctx.originalUrl.includes(item))) {
            resErr.e404()
            return
        }

        // 微服务初始化
        const micro = await useUserServiceClient()

        // 判断是否连接到 user 服务
        if (!micro.hasSocket('user')) {
            resErr.e403()
            return
        }

        // 验证token
        const res = await micro.request({
            service: 'user',
            path: '/tokenVerify',
            data: {
                method: 'SYS',
                header: ctx.request.header,
            }
        })

        if (res.ok !== 1) {
            ctx.status = res.status
            ctx.body = res
            return
        }

        await next()
    }
}