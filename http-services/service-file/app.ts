/*
 * @Description: 文件服务入口
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-20 16:28:38
 * @LastEditTime: 2022-08-02 11:21:48
 */
import Koa from 'koa'
import { useConfig, print } from 'tcp-micro-service'
import cors from 'koa2-cors'
import koaControl from 'koa-control'
import errorCatch from './middleware/errorCatch.js'
import permission from './middleware/permission.js'
import useBodyParserOpts from './hooks/useBodyParserOpts.js'
import Access from './controller/Access.js'
import Download from './controller/Download.js'
import Upload from './controller/Upload.js'

// 读取配置
const { server } = useConfig()

// 实例化
const app = new Koa()

// 是否为开发环境
const isDev = process.argv.includes('--dev')

// 开发环境允许跨域
if (isDev) {
    app.use(cors())
    //打印日志
    print.info('已允许跨域')
}

app
    // 前台错误捕获
    .use(errorCatch())
    // 权限验证
    .use(permission())
    // 控制器（含路由和bodyParser）
    .use(koaControl(app, {
        controller: [Access, Download, Upload],
        bodyParserOpts: useBodyParserOpts()
    }))
    // 服务启用
    .listen(server.port, () => {
        //打印端口信息
        print.info(`服务 ${server.name} 启动成功：http://127.0.0.1:${server.port}`)
    })
    // 后台错误捕获
    .on('error', (err) => {
        if (err.message.includes('already in use')) {
            throw new Error('端口被占用，请关闭相应服务或更改端口后重启')
        } else {
            print.error(err)
        }
    })

// export default app