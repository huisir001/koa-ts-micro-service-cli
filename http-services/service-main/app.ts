/*
 * @Description: 主服务启动
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-20 16:28:38
 * @LastEditTime: 2022-07-07 16:04:56
 */
import Koa from 'koa'
import { useConfig, print } from 'tcp-micro-service'
import cors from 'koa2-cors'
import errorCatch from './middleware/errorCatch.js'
import bodyParser from 'koa-body-2'
import useBodyParserOptions from './hooks/useBodyParserOptions.js'
import router from './router/index.js'

// 读取配置
const { server } = useConfig()

// 实例化
const app = new Koa()

// 是否为开发环境
const isDev = process.argv.includes('--dev')

// 开发环境允许跨域
if (isDev) {
    app.use(cors())
    //打印日志
    print.info('已允许跨域')
}

app
    // 前台错误捕获
    .use(errorCatch())
    // 处理请求体(入参)中间件
    .use(bodyParser(useBodyParserOptions()))
    // 路由挂载
    .use(router.routes())
    .use(router.allowedMethods())
    // 服务启用
    .listen(server.port, () => {
        //打印端口信息
        print.info(`服务 ${server.name} 启动成功：http://127.0.0.1:${server.port}`)
    })
    // 后台错误捕获
    .on('error', (err) => {
        if (err.message.includes('already in use')) {
            throw new Error('端口被占用，请关闭相应服务或更改端口后重启')
        } else {
            print.error(err)
        }
    })

// export default app