/*
 * @Description: bodyparser配置
 * @Autor: HuiSir<273250950@qq.com>
 * @Date: 2022-04-22 16:21:18
 * @LastEditTime: 2022-07-06 14:08:41
 */
import { type bodyParser } from 'koa-body-2'

export default (): bodyParser.IOptions => {
    return {
        multipart: false, // 不解析multipart数据，无法得到文件及fields参数，所有文件走文件服务
        jsonLimit: '1mb',
        formLimit: '56kb', // urlencoded,nodejs 默认64kb一个chunk,这里设置56kb 稍小一点
        textLimit: '56kb'
    }
}