/*
 * @Description: 请求验证、访问过滤
 * @Autor: HuiSir<www.zuifengyun.com>
 * @Date: 2022-05-25 13:48:57
 * @LastEditTime: 2022-07-22 17:00:55
 */
import type Router from '@koa/router'
import Micro, { useConfig, resErr } from 'tcp-micro-service'

export default (): Router.Middleware => {
    const { apiWhiteList, apiBlackList } = useConfig().common

    return async (ctx, next) => {
        // 白名单放行
        if (apiWhiteList?.find((item: string) => ctx.originalUrl.includes(item))) {
            await next() // 注意别忘了await
            return
        }

        // 黑名单阻断
        if (apiBlackList?.find((item: string) => ctx.originalUrl.includes(item))) {
            resErr.e404()
            return
        }

        // 判断是否连接user服务(登录接口白名单放行后，会连接到user服务，所以这里不必再连接)
        if (!(ctx.micro as Micro).hasSocket('user')) {
            resErr.e403()
            return
        }

        // 验证token
        const res = await (ctx.micro as Micro).request({
            service: 'user',
            path: '/tokenVerify',
            data: {
                method: 'SYS',
                header: ctx.request.header,
            }
        })

        if (res.ok !== 1) {
            ctx.status = res.status
            ctx.body = res
            return
        }

        await next()
    }
}