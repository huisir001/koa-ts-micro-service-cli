/*
 * @Description: 错误捕获
 * @Autor: HuiSir<www.zuifengyun.com>
 * @Date: 2022-05-19 16:12:51
 * @LastEditTime: 2022-07-22 18:00:20
 */
import type Koa from 'koa'
import { useResult, print } from 'tcp-micro-service'

const { error } = useResult()

export default () => {
    // 前台错误捕获
    const catchMidware: Koa.Middleware<Promise<void>> = async (ctx, next) => {
        try {
            // 返回格式,默认添加charset=utf-8
            // 对象默认为application/json，故此处可不配置
            // 后续有特例可在此判断重置
            ctx.type = 'application/json'
            await next()
        } catch (err: any) {
            //前台捕获
            const status = err.statusCode || 500
            ctx.status = status
            ctx.type = 'application/json'
            ctx.body = error({
                status,
                msg: err.message,
            })
            //后台捕获
            const statusWhiteList = [200, 400, 401, 403, 404, 405]
            //打印错误日志
            statusWhiteList.includes(status) || print.error(err)
        }
    }

    return catchMidware
}
